import { Document } from 'mongoose';

export default interface IDepartment extends Document {
  departmentName: string;
  phoneNumber?: string;
}
