import { DepartmentInput } from 'src/departments/dtos/departmentInput.dto';

export class UpdateDepartmentCommand {
  constructor(
    public readonly id: string,
    public readonly departmentInput: DepartmentInput,
  ) {}
}
