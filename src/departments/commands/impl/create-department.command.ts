import { DepartmentInput } from 'src/departments/dtos/departmentInput.dto';

export class CreateDepartmentCommand {
  constructor(public readonly departmentInput: DepartmentInput) {}
}
