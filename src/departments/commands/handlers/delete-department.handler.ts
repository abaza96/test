import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { DepartmentDeletedEvent } from 'src/departments/events/impl/department-deleted.event';
import { DepartmentRepository } from 'src/departments/repositories/department.repository';
import { DeleteDepartmentCommand } from '../impl/delete-department.command';

@CommandHandler(DeleteDepartmentCommand)
export class DeleteDepartmentHandler
  implements ICommandHandler<DeleteDepartmentCommand>
{
  constructor(
    private readonly repository: DepartmentRepository,
    private readonly eventBus: EventBus,
  ) {}
  async execute(command: DeleteDepartmentCommand) {
    const newDepartment = await this.repository.deleteDepartment(command.id);
    this.eventBus.publish(new DepartmentDeletedEvent(newDepartment));
    return newDepartment;
  }
}
