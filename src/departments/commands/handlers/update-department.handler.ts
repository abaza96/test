import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { DepartmentUpdatedEvent } from 'src/departments/events/impl/department-updated.event';
import { DepartmentRepository } from 'src/departments/repositories/department.repository';
import { UpdateDepartmentCommand } from '../impl/update-department.command';

@CommandHandler(UpdateDepartmentCommand)
export class UpdateDepartmentHandler
  implements ICommandHandler<UpdateDepartmentCommand>
{
  constructor(
    private readonly repository: DepartmentRepository,
    private readonly eventBus: EventBus,
  ) {}
  async execute(command: UpdateDepartmentCommand) {
    const updatedDepartment = await this.repository.updateDepartment(
      command.id,
      command.departmentInput,
    );
    this.eventBus.publish(new DepartmentUpdatedEvent(updatedDepartment));
    return updatedDepartment;
  }
}
