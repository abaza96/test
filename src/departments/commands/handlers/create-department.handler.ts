import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { DepartmentCreatedEvent } from 'src/departments/events/impl/department-created.event';
import { DepartmentRepository } from 'src/departments/repositories/department.repository';
import { CreateDepartmentCommand } from '../impl/create-department.command';

@CommandHandler(CreateDepartmentCommand)
export class CreateDepartmentHandler
  implements ICommandHandler<CreateDepartmentCommand>
{
  constructor(
    private readonly repository: DepartmentRepository,
    private readonly eventBus: EventBus,
  ) {}
  async execute(command: CreateDepartmentCommand) {
    const newDepartment = await this.repository.createDepartment(
      command.departmentInput,
    );
    this.eventBus.publish(new DepartmentCreatedEvent(newDepartment));
    return newDepartment;
  }
}
