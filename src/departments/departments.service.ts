import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import IDepartment from './interfaces/department.interface';
import { Model } from 'mongoose';
import { DepartmentInput } from './dtos/departmentInput.dto';
import IEmployee from 'src/employees/interfaces/employee.interface';

@Injectable()
export class DepartmentsService {
  constructor(
    @InjectModel('department')
    private readonly departmentModel: Model<IDepartment>,
    @InjectModel('employees')
    private readonly employeeModel: Model<IEmployee>,
  ) {}

  async getDepartments() {
    try {
      return await this.departmentModel.find();
    } catch (error) {
      throw error;
    }
  }
  async getDepartment(id: string) {
    try {
      return await this.departmentModel.findById(id);
    } catch (error) {
      throw error;
    }
  }

  async createDepartment(department: DepartmentInput) {
    try {
      return await this.departmentModel.create(department);
    } catch (error) {
      throw error;
    }
  }
  async updateDepartment(id: string, department: DepartmentInput) {
    try {
      return await this.departmentModel.findByIdAndUpdate(
        id,
        { $set: department },
        { new: true },
      );
    } catch (error) {
      throw error;
    }
  }
  async deleteDepartment(id: string) {
    try {
      const department = await this.departmentModel.findByIdAndRemove(id);
      //remove it from employees
      const employees = await this.employeeModel.find({ departments: id });
      employees.forEach((emp) => {
        const index = emp.departments.indexOf(id);
        if (index > -1) {
          emp.departments.splice(index, 1);
          emp.save();
        }
      });
      return department;
    } catch (error) {
      throw error;
    }
  }
  async departmentEmployees(id: string) {
    try {
      return await this.employeeModel
        .find({ departments: id })
        .populate('departments', 'departmentName')
        .exec();
    } catch (error) {
      throw error;
    }
  }
}
