import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class DepartmentInput {
  @Field()
  readonly departmentName: string;
  @Field({ nullable: true })
  readonly phoneNumber: string;
}
