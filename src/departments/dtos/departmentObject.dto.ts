import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class DepartmentObject {
  @Field(() => ID)
  readonly _id: string;
  @Field()
  readonly departmentName: string;
  @Field({ nullable: true })
  readonly phoneNumber: string;
}
