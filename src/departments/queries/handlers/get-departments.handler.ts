import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { DepartmentRepository } from 'src/departments/repositories/department.repository';
import { GetDepartmentsQuery } from '../impl/get-departments.query';

@QueryHandler(GetDepartmentsQuery)
export class GetDepartmentsHandler
  implements IQueryHandler<GetDepartmentsQuery>
{
  constructor(private readonly repository: DepartmentRepository) {}
  async execute(query: GetDepartmentsQuery) {
    return await this.repository.getDepartments();
  }
}
