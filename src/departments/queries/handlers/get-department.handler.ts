import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { DepartmentRepository } from 'src/departments/repositories/department.repository';
import { GetDepartmentQuery } from '../impl/get-department.query';

@QueryHandler(GetDepartmentQuery)
export class GetDepartmentHandler implements IQueryHandler<GetDepartmentQuery> {
  constructor(private readonly repository: DepartmentRepository) {}
  async execute(query: GetDepartmentQuery) {
    return await this.repository.getDepartment(query.id);
  }
}
