import { GetDepartmentHandler } from './get-department.handler';
import { GetDepartmentsHandler } from './get-departments.handler';

export const DepartmentQueryHandlers = [
  GetDepartmentsHandler,
  GetDepartmentHandler,
];
