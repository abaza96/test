import * as mongoose from 'mongoose';
import IDepartment from '../interfaces/department.interface';

export const departmentSchema = new mongoose.Schema<IDepartment>({
  departmentName: String,
  phoneNumber: {
    type: String,
    required: false,
  },
});
