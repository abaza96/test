import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { EmployeeObject } from 'src/employees/dtos/employeObject.dto';
import { CreateDepartmentCommand } from '../commands/impl/create-department.command';
import { DeleteDepartmentCommand } from '../commands/impl/delete-department.command';
import { UpdateDepartmentCommand } from '../commands/impl/update-department.command';
import { DepartmentsService } from '../departments.service';
import { DepartmentInput } from '../dtos/departmentInput.dto';
import { DepartmentObject } from '../dtos/departmentObject.dto';
import { GetDepartmentQuery } from '../queries/impl/get-department.query';
import { GetDepartmentsQuery } from '../queries/impl/get-departments.query';

@Resolver()
export class DepartmentsResolver {
  constructor(
    private readonly departmentService: DepartmentsService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Query(() => [DepartmentObject])
  async departments() {
    return await this.queryBus.execute(new GetDepartmentsQuery());
  }

  @Query(() => DepartmentObject)
  async department(@Args('id') id: string) {
    return await this.queryBus.execute(new GetDepartmentQuery(id));
  }

  @Query(() => [EmployeeObject])
  async departmentEmployees(@Args('id') id: string) {
    return await this.departmentService.departmentEmployees(id);
  }

  @Mutation(() => DepartmentObject)
  async createDepartment(@Args('department') department: DepartmentInput) {
    return await this.commandBus.execute(
      new CreateDepartmentCommand(department),
    );
  }

  @Mutation(() => DepartmentObject)
  async updateDepartment(
    @Args('id') id: string,
    @Args('department') department: DepartmentInput,
  ) {
    return await this.commandBus.execute(
      new UpdateDepartmentCommand(id, department),
    );
  }

  @Mutation(() => DepartmentObject)
  async deleteDepartment(@Args('id') id: string) {
    return await this.commandBus.execute(new DeleteDepartmentCommand(id));
  }
}
