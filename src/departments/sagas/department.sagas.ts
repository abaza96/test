import { Injectable } from '@nestjs/common';
import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable, delay, map } from 'rxjs';
import { DepartmentCreatedEvent } from '../events/impl/department-created.event';
import { DepartmentDeletedEvent } from '../events/impl/department-deleted.event';
import { DepartmentUpdatedEvent } from '../events/impl/department-updated.event';

@Injectable()
export class DepartmentSagas {
  @Saga()
  departmentCreated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DepartmentCreatedEvent),
      delay(1000),
      map((event) => {
        console.log('Inside [DepartmentCreated] Saga');
        return null;
      }),
    );
  };

  @Saga()
  departmentUpdated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DepartmentUpdatedEvent),
      delay(1000),
      map((event) => {
        console.log('Inside [DepartmentUpdated] Saga');
        return null;
      }),
    );
  };

  @Saga()
  departmentDeleted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(DepartmentDeletedEvent),
      delay(1000),
      map((event) => {
        console.log('Inside [DepartmentDeleted] Saga');
        return null;
      }),
    );
  };
}
