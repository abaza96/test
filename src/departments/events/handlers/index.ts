import { DepartmentCreatedHandler } from './department-created.handler';
import { DepartmentDeletedHandler } from './department-deleted.handler';
import { DepartmentUpdatedHandler } from './department-updated.handler';

export const DepartmentEventHandlers = [
  DepartmentCreatedHandler,
  DepartmentDeletedHandler,
  DepartmentUpdatedHandler,
];
