import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DepartmentDeletedEvent } from '../impl/department-deleted.event';

@EventsHandler(DepartmentDeletedEvent)
export class DepartmentDeletedHandler
  implements IEventHandler<DepartmentDeletedEvent>
{
  handle(event: DepartmentDeletedEvent) {
    console.log('Department Deleted');
  }
}
