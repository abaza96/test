import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DepartmentCreatedEvent } from '../impl/department-created.event';

@EventsHandler(DepartmentCreatedEvent)
export class DepartmentCreatedHandler
  implements IEventHandler<DepartmentCreatedEvent>
{
  handle(event: DepartmentCreatedEvent) {
    console.log('New Department Created');
  }
}
