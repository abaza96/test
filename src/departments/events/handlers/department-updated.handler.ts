import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DepartmentUpdatedEvent } from '../impl/department-updated.event';

@EventsHandler(DepartmentUpdatedEvent)
export class DepartmentUpdatedHandler
  implements IEventHandler<DepartmentUpdatedEvent>
{
  handle(event: DepartmentUpdatedEvent) {
    console.log('Department Updated');
  }
}
