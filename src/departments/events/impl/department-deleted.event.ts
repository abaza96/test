import IDepartment from 'src/departments/interfaces/department.interface';

export class DepartmentDeletedEvent {
  constructor(public readonly department: IDepartment) {}
}
