import IDepartment from 'src/departments/interfaces/department.interface';

export class DepartmentCreatedEvent {
  constructor(public readonly department: IDepartment) {}
}
