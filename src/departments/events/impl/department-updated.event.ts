import IDepartment from 'src/departments/interfaces/department.interface';

export class DepartmentUpdatedEvent {
  constructor(public readonly department: IDepartment) {}
}
