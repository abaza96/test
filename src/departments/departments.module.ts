import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { EmployeesService } from 'src/employees/employees.service';
import { employeeSchema } from 'src/employees/models/employees.schema';
import { DepartmentCommandHandlers } from './commands/handlers';
import { DepartmentsController } from './departments.controller';
import { DepartmentsService } from './departments.service';
import { DepartmentEventHandlers } from './events/handlers';
import { DepartmentsResolver } from './graphql/departments.resolver';
import { departmentSchema } from './models/departments.schema';
import { DepartmentQueryHandlers } from './queries/handlers';
import { DepartmentRepository } from './repositories/department.repository';
import { DepartmentSagas } from './sagas/department.sagas';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'department', schema: departmentSchema },
    ]),
    MongooseModule.forFeature([{ name: 'employees', schema: employeeSchema }]),
    CqrsModule,
  ],
  controllers: [DepartmentsController],
  providers: [
    DepartmentsService,
    DepartmentsResolver,
    EmployeesService,
    ...DepartmentCommandHandlers,
    ...DepartmentEventHandlers,
    ...DepartmentQueryHandlers,
    DepartmentRepository,
    DepartmentSagas,
  ],
})
export class DepartmentsModule {}
