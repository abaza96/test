import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { MongooseModule } from '@nestjs/mongoose';
import { EmployeeCommandHandlers } from './commands/handlers';
import { EmployeesController } from './employees.controller';
import { EmployeesService } from './employees.service';
import { EmployeeEventHandlers } from './events/handlers';
import { EmployeesResolver } from './graphql/employees.resolver';
import { employeeSchema } from './models/employees.schema';
import { EmployeeQueryHandlers } from './queries/handlers';
import { EmployeeRepository } from './repositories/employee.repository';
import { EmployeeSagas } from './sagas/employee.sagas';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'employees', schema: employeeSchema }]),
    CqrsModule,
  ],
  controllers: [EmployeesController],
  providers: [
    EmployeesService,
    EmployeesResolver,
    ...EmployeeCommandHandlers,
    ...EmployeeEventHandlers,
    ...EmployeeQueryHandlers,
    EmployeeRepository,
    EmployeeSagas,
  ],
})
export class EmployeesModule {}
