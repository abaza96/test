import IEmployee from 'src/employees/interfaces/employee.interface';

export class EmployeeCreatedEvent {
  constructor(public readonly department: IEmployee) {}
}
