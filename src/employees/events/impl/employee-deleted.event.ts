import IEmployee from 'src/employees/interfaces/employee.interface';

export class EmployeeDeletedEvent {
  constructor(public readonly department: IEmployee) {}
}
