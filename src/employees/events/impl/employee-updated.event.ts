import IEmployee from 'src/employees/interfaces/employee.interface';

export class EmployeeUpdatedEvent {
  constructor(public readonly department: IEmployee) {}
}
