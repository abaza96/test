import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { EmployeeUpdatedEvent } from '../impl/employee-updated.event';

@EventsHandler(EmployeeUpdatedEvent)
export class EmployeeUpdatedHandler
  implements IEventHandler<EmployeeUpdatedEvent>
{
  handle(event: EmployeeUpdatedEvent) {
    console.log('Employee Updated');
  }
}
