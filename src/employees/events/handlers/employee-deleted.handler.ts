import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { EmployeeDeletedEvent } from '../impl/employee-deleted.event';

@EventsHandler(EmployeeDeletedEvent)
export class EmployeeDeletedHandler
  implements IEventHandler<EmployeeDeletedEvent>
{
  handle(event: EmployeeDeletedEvent) {
    console.log('Employee Deleted');
  }
}
