import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { EmployeeCreatedEvent } from '../impl/employee-created.event';

@EventsHandler(EmployeeCreatedEvent)
export class EmployeeCreatedHandler
  implements IEventHandler<EmployeeCreatedEvent>
{
  handle(event: EmployeeCreatedEvent) {
    console.log('New Employee Created');
  }
}
