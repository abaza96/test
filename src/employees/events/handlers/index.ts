import { EmployeeCreatedHandler } from './employee-created.handler';
import { EmployeeDeletedHandler } from './employee-deleted.handler';
import { EmployeeUpdatedHandler } from './employee-updated.handler';

export const EmployeeEventHandlers = [
  EmployeeCreatedHandler,
  EmployeeDeletedHandler,
  EmployeeUpdatedHandler,
];
