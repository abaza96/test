import mongoose from 'mongoose';
import IEmployee from '../interfaces/employee.interface';

export const employeeSchema = new mongoose.Schema<IEmployee>({
  employeeId: String,
  firstName: String,
  lastName: String,
  age: Number,
  monthlySalary: Number,
  departments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'department' }],
});
