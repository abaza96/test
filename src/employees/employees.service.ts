import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import IEmployee from './interfaces/employee.interface';
import { Model } from 'mongoose';
import { EmployeeInput } from './dtos/employeeInput.dto';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectModel('employees') private readonly employeeModel: Model<IEmployee>,
  ) {}

  async getEmployees() {
    try {
      return await this.employeeModel
        .find()
        .populate('departments', 'departmentName')
        .exec();
    } catch (error) {
      throw error;
    }
  }
  async getEmployee(id: string) {
    try {
      return await this.employeeModel
        .findById(id)
        .populate('departments', 'departmentName')
        .exec();
    } catch (error) {
      throw error;
    }
  }
  async createEmployee(employeeInput: EmployeeInput) {
    try {
      let employee = await this.employeeModel.create(employeeInput);
      employee = await employee.populate('departments', 'departmentName');
      return employee;
    } catch (error) {
      throw error;
    }
  }
  async updateEmployee(id: string, employeeInput: EmployeeInput) {
    try {
      let employee = await this.employeeModel.findByIdAndUpdate(
        id,
        { $set: employeeInput },
        { new: true },
      );
      employee = await employee.populate('departments', 'departmentName');
      return employee;
    } catch (error) {
      throw error;
    }
  }
  async deleteEmployee(id: string) {
    try {
      const employee = await this.employeeModel
        .findByIdAndRemove(id)
        .populate('departments', 'departmentName');
      return employee;
    } catch (error) {
      throw error;
    }
  }
}
