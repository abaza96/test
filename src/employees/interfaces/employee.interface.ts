import { Document } from 'mongoose';

export default interface IEmployee extends Document {
  employeeId: string;
  firstName: string;
  lastName: string;
  age: number;
  monthlySalary: number;
  departments: string[];
}
