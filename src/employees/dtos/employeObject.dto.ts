import { Field, Float, ID, Int, ObjectType } from '@nestjs/graphql';
import { DepartmentObject } from 'src/departments/dtos/departmentObject.dto';
import IDepartment from 'src/departments/interfaces/department.interface';

@ObjectType()
export class EmployeeObject {
  @Field(() => ID)
  readonly _id: string;
  @Field()
  readonly firstName: string;
  @Field()
  readonly lastName: string;
  @Field(() => Int)
  readonly age: number;
  @Field(() => Float)
  readonly monthlySalary: number;
  @Field(() => [DepartmentObject], { nullable: 'itemsAndList' })
  readonly departments: [IDepartment];
}
