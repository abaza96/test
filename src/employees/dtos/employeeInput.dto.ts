import { Field, Float, InputType, Int } from '@nestjs/graphql';

@InputType()
export class EmployeeInput {
  @Field()
  readonly firstName: string;
  @Field()
  readonly lastName: string;
  @Field(() => Int)
  readonly age: number;
  @Field(() => Float)
  readonly monthlySalary: number;
  @Field(() => [String], { nullable: 'itemsAndList' })
  readonly departments?: string[];
}
