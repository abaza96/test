import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { EmployeeInput } from '../dtos/employeeInput.dto';
import { EmployeeObject } from '../dtos/employeObject.dto';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { GetEmployeesQuery } from '../queries/impl/get-employees.query';
import { GetEmployeeQuery } from '../queries/impl/get-employee.query';
import { CreateEmployeeCommand } from '../commands/impl/create-employee.command';
import { UpdateEmployeeCommand } from '../commands/impl/update-employee.command';
import { DeleteEmployeeCommand } from '../commands/impl/delete-employee.command';

@Resolver()
export class EmployeesResolver {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Query(() => [EmployeeObject])
  async employees() {
    return await this.queryBus.execute(new GetEmployeesQuery());
  }

  @Query(() => EmployeeObject)
  async employee(@Args('id') id: string) {
    return await this.queryBus.execute(new GetEmployeeQuery(id));
  }

  @Mutation(() => EmployeeObject)
  async createEmployee(@Args('employee') employee: EmployeeInput) {
    return await this.commandBus.execute(new CreateEmployeeCommand(employee));
  }

  @Mutation(() => EmployeeObject)
  async updateEmployee(
    @Args('id') id: string,
    @Args('employee') employee: EmployeeInput,
  ) {
    return await this.commandBus.execute(
      new UpdateEmployeeCommand(id, employee),
    );
  }

  @Mutation(() => EmployeeObject)
  async deleteEmployee(@Args('id') id: string) {
    return await this.commandBus.execute(new DeleteEmployeeCommand(id));
  }
}
