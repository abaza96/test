import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { EmployeeRepository } from 'src/employees/repositories/employee.repository';
import { GetEmployeesQuery } from '../impl/get-employees.query';

@QueryHandler(GetEmployeesQuery)
export class GetEmployeesHandler implements IQueryHandler<GetEmployeesQuery> {
  constructor(private readonly repository: EmployeeRepository) {}
  async execute(query: GetEmployeesQuery) {
    return await this.repository.getEmployees();
  }
}
