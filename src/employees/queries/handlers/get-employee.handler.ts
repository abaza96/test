import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { EmployeeRepository } from 'src/employees/repositories/employee.repository';
import { GetEmployeeQuery } from '../impl/get-employee.query';

@QueryHandler(GetEmployeeQuery)
export class GetEmployeeHandler implements IQueryHandler<GetEmployeeQuery> {
  constructor(private readonly repository: EmployeeRepository) {}
  async execute(query: GetEmployeeQuery) {
    return await this.repository.getEmployee(query.id);
  }
}
