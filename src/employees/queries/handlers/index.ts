import { GetEmployeeHandler } from './get-employee.handler';
import { GetEmployeesHandler } from './get-employees.handler';

export const EmployeeQueryHandlers = [GetEmployeesHandler, GetEmployeeHandler];
