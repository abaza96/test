import { CreateEmployeeHandler } from './create-employee.handler';
import { DeleteEmployeeHandler } from './delete-employee.handler';
import { UpdateEmployeeHandler } from './update-employee.handler';

export const EmployeeCommandHandlers = [
  CreateEmployeeHandler,
  UpdateEmployeeHandler,
  DeleteEmployeeHandler,
];
