import { CommandHandler, ICommandHandler, EventBus } from '@nestjs/cqrs';
import { EmployeeUpdatedEvent } from 'src/employees/events/impl/employee-updated.event';
import { EmployeeRepository } from 'src/employees/repositories/employee.repository';
import { UpdateEmployeeCommand } from '../impl/update-employee.command';

@CommandHandler(UpdateEmployeeCommand)
export class UpdateEmployeeHandler
  implements ICommandHandler<UpdateEmployeeCommand>
{
  constructor(
    private readonly repository: EmployeeRepository,
    private readonly eventBus: EventBus,
  ) {}
  async execute(command: UpdateEmployeeCommand) {
    const updatedEmployee = await this.repository.updateEmployee(
      command.id,
      command.employeeInput,
    );
    this.eventBus.publish(new EmployeeUpdatedEvent(updatedEmployee));
    return updatedEmployee;
  }
}
