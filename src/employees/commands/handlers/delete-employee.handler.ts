import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { EmployeeDeletedEvent } from 'src/employees/events/impl/employee-deleted.event';
import { EmployeeRepository } from 'src/employees/repositories/employee.repository';
import { DeleteEmployeeCommand } from '../impl/delete-employee.command';

@CommandHandler(DeleteEmployeeCommand)
export class DeleteEmployeeHandler
  implements ICommandHandler<DeleteEmployeeCommand>
{
  constructor(
    private readonly repository: EmployeeRepository,
    private readonly eventBus: EventBus,
  ) {}
  async execute(command: DeleteEmployeeCommand) {
    const newEmployee = await this.repository.deleteEmployee(command.id);
    this.eventBus.publish(new EmployeeDeletedEvent(newEmployee));
    return newEmployee;
  }
}
