import { CommandHandler, EventBus, ICommandHandler } from '@nestjs/cqrs';
import { EmployeeCreatedEvent } from 'src/employees/events/impl/employee-created.event';
import { EmployeeRepository } from 'src/employees/repositories/employee.repository';
import { CreateEmployeeCommand } from '../impl/create-employee.command';

@CommandHandler(CreateEmployeeCommand)
export class CreateEmployeeHandler
  implements ICommandHandler<CreateEmployeeCommand>
{
  constructor(
    private readonly repository: EmployeeRepository,
    private readonly eventBus: EventBus,
  ) {}
  async execute(command: CreateEmployeeCommand) {
    const newEmployee = await this.repository.createEmployee(
      command.employeeInput,
    );
    this.eventBus.publish(new EmployeeCreatedEvent(newEmployee));
    return newEmployee;
  }
}
