import { EmployeeInput } from 'src/employees/dtos/employeeInput.dto';

export class CreateEmployeeCommand {
  constructor(public readonly employeeInput: EmployeeInput) {}
}
