import { EmployeeInput } from 'src/employees/dtos/employeeInput.dto';

export class UpdateEmployeeCommand {
  constructor(
    public readonly id: string,
    public readonly employeeInput: EmployeeInput,
  ) {}
}
