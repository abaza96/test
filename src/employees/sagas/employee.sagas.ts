import { Injectable } from '@nestjs/common';
import { Saga, ICommand, ofType } from '@nestjs/cqrs';
import { Observable, delay, map } from 'rxjs';
import { EmployeeCreatedEvent } from '../events/impl/employee-created.event';
import { EmployeeDeletedEvent } from '../events/impl/employee-deleted.event';
import { EmployeeUpdatedEvent } from '../events/impl/employee-updated.event';

@Injectable()
export class EmployeeSagas {
  @Saga()
  employeeCreated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(EmployeeCreatedEvent),
      delay(1000),
      map((event) => {
        console.log('Inside [EmployeeCreated] Saga');
        return null;
      }),
    );
  };

  @Saga()
  employeeUpdated = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(EmployeeUpdatedEvent),
      delay(1000),
      map((event) => {
        console.log('Inside [EmployeeUpdated] Saga');
        return null;
      }),
    );
  };

  @Saga()
  employeeDeleted = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(EmployeeDeletedEvent),
      delay(1000),
      map((event) => {
        console.log('Inside [EmployeeDeleted] Saga');
        return null;
      }),
    );
  };
}
